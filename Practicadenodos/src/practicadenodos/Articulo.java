/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicadenodos;

/**
 *
 * @author Clite
 */
public class Articulo {
    
     private String referencia;
    private String descripcion;
    private String precio;
    private String itbis;
    private String cantidad;
    private String categoria;

    public Articulo() {
    }

    public void dar(String referencia, String descripcion, String precio, String itbis, String catidad, String categoria) {
        this.referencia = referencia;
        this.descripcion = descripcion;
        this.precio = precio;
        this.itbis = itbis;
        this.cantidad = catidad;
        this.categoria = categoria;
    }
public String gettodo() {
        return (referencia+";"+descripcion+";"+precio+";"+itbis+";"+cantidad+";"+categoria);
    }
    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getItbis() {
        return itbis;
    }

    public void setItbis(String itbis) {
        this.itbis = itbis;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    
}
